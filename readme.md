[Official Documentation](https://github.com/intuit/karate)

## Scenario Outline

https://github.com/intuit/karate/blob/master/README.md#scenario-outline-enhancements

## Code Reuse 

https://github.com/intuit/karate/blob/master/README.md#calling-other-feature-files


## Command line - Examples

Specific Feature Example

```
mvn clean test -Dkarate.options="--tags ~@ignore classpath:v1/product-types/create-product-type.feature" -Dtest=CustomTest
```

Multiple Tags

```
mvn clean test -Dkarate.options="--tags @400 --tags ~@ignore classpath:v1/product-types/create-product-type.feature" -Dtest=CustomTest
```

Execute a folder

```
mvn clean test -Dkarate.options="--tags ~@ignore classpath:v1/product-types/" -Dtest=CustomTest
```
