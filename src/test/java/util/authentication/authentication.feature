Feature: Authentication

  Background:
    * url auth.baseUrl

  @ignore @authentication
  Scenario: Authentication | Status 200 | Return an access token
    * set auth.baseUrl = null
    Given path 'oauth', 'token'
    And form fields auth
    When method post
    Then status 200