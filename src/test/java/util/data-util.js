function fn(){
    var default_max = 9999999;
    var _random_number = function(max) { return Math.floor(Math.random() * max)  };

    var functions = {
        random_number: function(max) {
            if ( max === undefined ){
                max = default_max;
            }
            return _random_number(max);
        },
        random_text: function() {
            return 'Test ' + _random_number(default_max);
        },
        random_uuid: function() {
            return java.util.UUID.randomUUID() + '';
        },
        generate_string: function(length, character) {
            return new Array(length + 1).join( character );
        }
    }

    return functions;
}