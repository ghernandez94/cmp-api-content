Feature: Test Companies

  Background:
    * url baseUrl
    * def data_utilities = call read("classpath:util/data-util.js")

  @ignore @reusable_creation
  Scenario: Create a Company | Status 201
    * def random_str = data_utilities.random_text()
    * def new_company = 
    """
    {
        "name": "#(random_str)",
        "nationalId": [
            {
                "value": "#(random_str)",
                "country": "CL"
            }
        ]
    }
    """

    Given path 'v1', 'companies'
    And request new_company
    When method post
    Then status 201