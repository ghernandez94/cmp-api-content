function fn() {    
    var config = {
        auth: {}
    };

    karate.configure('readTimeout', 300000);
    karate.configure('retry', { count: 3, interval: 3000 });
    karate.configure('ssl', false);

    config.baseUrl = 'https://api.test.falabella-marketplace.services';
    config.auth.baseUrl = 'https://cmp-falabella.auth0.com';
    config.auth.client_id = '[client_id]';
    config.auth.client_secret = '[client_secret]';
    config.auth.audience = 'https://api.test.falabella-marketplace.services';
    config.auth.grant_type = 'http://auth0.com/oauth/grant-type/password-realm';
    config.auth.username = '[username]';
    config.auth.password = '[password]';
    config.auth.realm = 'User-Password-Test';
    var authResponse = karate.callSingle('classpath:util/authentication/authentication.feature', config);
    config.access_token = authResponse.response.access_token;

    // Global authentication
    karate.configure('headers', { 'Authorization': 'Bearer ' + config.access_token });

    return config;
}