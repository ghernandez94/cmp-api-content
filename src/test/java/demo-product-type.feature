Feature: Test Product Types

  Background:
    * url 'https://api.test.falabella-marketplace.services'

  Scenario: Get Product Type by ID | Status 200
    * def id = '021f6790-883c-4fe3-9a9b-a4efe2c1a120'

    Given path 'v1', 'product-types', id
    When method get
    Then status 200
    And match response.id == id

  Scenario: Get Product Type by ID | Status 400 | UUID Format
    * def id = 'test-1234'
    Given path 'v1', 'product-types', id
    When method get
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'