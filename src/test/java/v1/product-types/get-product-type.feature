Feature: Test Product Types

  Background:
    * url baseUrl
    * def data_utilities = call read("classpath:util/data-util.js")

  Scenario: Get Product Type by ID | Status 200
    # Create a product type
    * def id_product_type = karate.call('create-product-type.feature@reusable_creation').response.id

    Given path 'v1', 'product-types', id_product_type
    When method get
    Then status 200
    And match response.id == id_product_type

  Scenario: Get Product Type by ID | Status 400 | UUID format
    * def id = 'test-1234'
    Given path 'v1', 'product-types', id
    When method get
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'

  Scenario: Get Product Type by ID | Status 404 | Product type ID doesn't exist
    * def id = data_utilities.random_uuid()
    Given path 'v1', 'product-types', id
    When method get
    Then status 404
    And match response.message == 'Resource not found'