Feature: Test Product Types

    Background:
        * url baseUrl

    @GetAll
    Scenario: Get all product types | Status 200
        Given path 'v1', 'product-types'
        When method get
        Then status 200
        And match response.data == '#array'

    Scenario: Get all product types | Status 400 | Negative parameters
        Given path 'v1', 'product-types'
        And param page = -1
        And param limit = -1
        When method get
        Then status 400
        And match response.message == 'Bad request parameters'
        And match response.errors == '#array'