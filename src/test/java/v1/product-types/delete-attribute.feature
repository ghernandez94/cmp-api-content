Feature: Test Product Type Attributes

  Background:
    * url baseUrl
    * def data_utilities = call read("classpath:util/data-util.js")

  @204
  Scenario: Delete an Attribute | Status 204
    # Create an Attribute
    * def feature_attribute = karate.call('create-attribute.feature@reusable_creation')
    * def id_product_type = feature_attribute.id_product_type
    * def id_attribute = feature_attribute.response.id

    Given path 'v1', 'product-types', id_product_type, 'attributes', id_attribute
    When method delete
    Then status 204

  @400
  Scenario: Delete an Attribute | Status 400 | Non-existent attribute
    # Create a product type
    * def id_product_type = karate.call('create-product-type.feature@reusable_creation').response.id
    * def random_attribute = data_utilities.random_uuid()

    Given path 'v1', 'product-types', id_product_type, 'attributes', random_attribute
    When method delete
    Then status 404
    And match response.message == 'Resource not found'

  @400
  Scenario: Delete an Attribute | Status 400 | UUID format
    # Create a product type
    * def id_product_type = karate.call('create-product-type.feature@reusable_creation').response.id
    * def id_attribute = 'test-123'

    Given path 'v1', 'product-types', id_product_type, 'attributes', id_attribute
    When method delete
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'