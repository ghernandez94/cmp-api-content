Feature: Test Product Type Attributes

  Background:
    * url baseUrl
    * def create_201 = karate.callSingle('data/create-attribute-201.js')
    * def create_400 = karate.callSingle('data/create-attribute-400.js')

  @ignore @reusable_creation
  Scenario: Create an Attribute | Status 201
    # Create a product type
    * def id_product_type = karate.call('create-product-type.feature@reusable_creation').response.id
    * def new_attribute = create_201[0].body
    
    Given path 'v1', 'product-types', id_product_type, 'attributes'
    And request new_attribute
    When method post
    Then status 201

  @201
  Scenario Outline: Create an Attribute | Status 201 | <case-description>
    # Create a product type
    * def id_product_type = karate.call('create-product-type.feature@reusable_creation').response.id

    Given path 'v1', 'product-types', id_product_type, 'attributes'
    And request <body>
    When method post
    Then status 201
    Examples:
      | create_201 |

  @400
  Scenario Outline: Create an Attribute | Status 400 | <case-description>
    # Create a product type
    * def id_product_type = karate.call('create-product-type.feature@reusable_creation').response.id

    Given path 'v1', 'product-types', id_product_type, 'attributes'
    And request <body>
    When method post
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'
    Examples:
      | create_400 |