function fn(){
    var data_util = karate.call('classpath:util/data-util.js');
    var result = [];
    var random_str = '';

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "All fields",
            "body": {
                "type": {
                    "name": "text"
                },
                "name": random_str,
                "label": random_str,
                "attributeConstraint": "None",
                "isRequired": true,
                "isSearchable": true,
                "isRecommended": false
            }
        }
    );

    return result;
}