function fn(){
    var data_util = karate.call('classpath:util/data-util.js');
    var result = [];
    var random_str = '';

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Missing require attribute type field",
            "body": {
                "type": {

                },
                "name": random_str,
                "label": random_str,
                "attributeConstraint": "None",
                "isRequired": true,
                "isSearchable": true,
                "isRecommended": false
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Missing attribute type required fields when it's an enum type.",
            "body": {
                "type": {
                    "name": "enum",
                    "values": [
                        {}
                    ]
                },
                "name": random_str,
                "label": random_str,
                "attributeConstraint": "None",
                "isRequired": true,
                "isSearchable": true,
                "isRecommended": false
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Wrong data types (a string instead of a boolean)",
            "body": {
                "type": {
                    "name": "text"
                },
                "name": random_str,
                "label": random_str,
                "attributeConstraint": "None",
                "isRequired": "WrongDataType",
                "isSearchable": "WrongDataType",
                "isRecommended": false
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Disallowed values (disallowed type name)",
            "body": {
                "type": {
                    "name": "DisallowedValue"
                },
                "name": random_str,
                "label": random_str,
                "attributeConstraint": "None",
                "isRequired": true,
                "isSearchable": true,
                "isRecommended": false
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Disallowed values (disallowed attribute constraint)",
            "body": {
                "type": {
                    "name": "text"
                },
                "name": random_str,
                "label": random_str,
                "attributeConstraint": "DisallowedValue",
                "isRequired": true,
                "isSearchable": true,
                "isRecommended": false
            }
        }
    );

    return result;
}