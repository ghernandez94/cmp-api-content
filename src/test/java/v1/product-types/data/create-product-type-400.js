function fn(){
    var data_util = karate.call('classpath:util/data-util.js');
    var result = [];
    var random_str = '';

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Missing required field (name)",
            "body": {
                "description": random_str
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Missing require attribute field",
            "body": {
                "name": random_str,
                "description": random_str,
                "attributes" : [
                    {
                    }
                ]
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Missing require attribute type field",
            "body": {
                "name": random_str,
                "description": random_str,
                "attributes" : [
                    {
                        "type": {

                        },
                        "name": random_str,
                        "label": random_str,
                        "attributeConstraint": "None",
                        "isRequired": true,
                        "isSearchable": true,
                        "isRecommended": false
                    }
                ]
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Missing attribute type required fields when it's an enum type.",
            "body": {
                "name": random_str,
                "description": random_str,
                "attributes" : [
                    {
                        "type": {
                            "name": "enum",
                            "values": [
                                {}
                            ]
                        },
                        "name": random_str,
                        "label": random_str,
                        "attributeConstraint": "None",
                        "isRequired": true,
                        "isSearchable": true,
                        "isRecommended": false
                    }
                ]
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Wrong data types (a string instead of a boolean)",
            "body": {
                "name": random_str,
                "description": random_str,
                "attributes" : [
                    {
                        "type": {
                            "name": "text"
                        },
                        "name": random_str,
                        "label": random_str,
                        "attributeConstraint": "None",
                        "isRequired": "WrongDataType",
                        "isSearchable": "WrongDataType",
                        "isRecommended": false
                    }
                ]
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Disallowed values (disallowed type name)",
            "body": {
                "name": random_str,
                "description": random_str,
                "attributes" : [
                    {
                        "type": {
                            "name": "DisallowedValue"
                        },
                        "name": random_str,
                        "label": random_str,
                        "attributeConstraint": "None",
                        "isRequired": true,
                        "isSearchable": true,
                        "isRecommended": false
                    }
                ]
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Disallowed values (disallowed attribute constraint)",
            "body": {
                "name": random_str,
                "description": random_str,
                "attributes" : [
                    {
                        "type": {
                            "name": "text"
                        },
                        "name": random_str,
                        "label": random_str,
                        "attributeConstraint": "DisallowedValue",
                        "isRequired": true,
                        "isSearchable": true,
                        "isRecommended": false
                    }
                ]
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Invalid length (description)",
            "body": {
                "name": random_str,
                "description": data_util.generate_string(260, 'c')
            }
        }
    );

    return result;
}