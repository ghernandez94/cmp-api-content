Feature: Test Product Types

  Background:
    * url baseUrl
    * def create_201 = karate.callSingle('data/create-product-type-201.js')
    * def create_400 = karate.callSingle('data/create-product-type-400.js')

  @ignore @reusable_creation
  Scenario: Create a Product Type | Status 201
    * def new_product_type = create_201[0].body
    Given path 'v1', 'product-types'
    And request new_product_type
    When method post
    Then status 201
    
  @201 @Smoke
  Scenario Outline: Create a Product Type | Status 201 | <case-description>
    Given path 'v1', 'product-types'
    And request <body>
    When method post
    Then status 201
    Examples:
      | create_201 |

  @400
  Scenario Outline: Create a Product Type | Status 400 | <case-description>
    Given path 'v1', 'product-types'
    And request <body>
    When method post
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'
    Examples:
      | create_400 |