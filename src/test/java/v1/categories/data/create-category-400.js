function fn(){
    var data_util = karate.call('classpath:util/data-util.js');
    var tenant = karate.call("classpath:util/companies/create-company.feature").response.id;
    var random_tenant =  data_util.random_uuid();
    var result = [];
    var random_str = '';

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Missing required field (name)",
            "tenant": tenant,
            "body": {
                "description": random_str
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Empty required fields (name and description)",
            "tenant": tenant,
            "body": {
                "name": "",
                "description": ""
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Wrong order value. (string instead of a number)",
            "tenant": tenant,
            "body": {
                "key": random_str,
                "name": random_str,
                "slug": random_str,
                "description": random_str,
                "metaTitle": random_str,
                "metaDescription": random_str,
                "metaKeywords": random_str,
                "order": random_str
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Wrong parent id value. (string instead of a UUID)",
            "tenant": tenant,
            "body": {
                "key": random_str,
                "name": random_str,
                "slug": random_str,
                "description": random_str,
                "metaTitle": random_str,
                "metaDescription": random_str,
                "metaKeywords": random_str,
                "order": 0,
                "parent": {
                    "id": "NOTVALIDID"
                }
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Parent ID non-existent",
            "tenant": tenant,
            "body": {
                "key": random_str,
                "name": random_str,
                "slug": random_str,
                "description": random_str,
                "metaTitle": random_str,
                "metaDescription": random_str,
                "metaKeywords": random_str,
                "order": 0,
                "parent": {
                    "id": random_tenant
                }
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Non-existent Tenant",
            "tenant": random_tenant,
            "body": {
                "name": random_str,
                "description": random_str,
            }
        }
    );

    return result;
}