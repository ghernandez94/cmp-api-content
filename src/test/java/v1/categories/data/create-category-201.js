function fn(){
    var data_util = karate.call('classpath:util/data-util.js');
    var tenant = karate.call("classpath:util/companies/create-company.feature").response.id;
    var result = [];
    var random_str = '';

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "Just required fields",
            "tenant": tenant,
            "body": {
                "name": random_str,
                "description": random_str,
            }
        }
    );

    random_str = data_util.random_text();
    karate.appendTo(result,
        {
            "case-description": "All fields",
            "tenant": tenant,
            "body": {
                "key": random_str,
                "name": random_str,
                "slug": random_str,
                "description": random_str,
                "metaTitle": random_str,
                "metaDescription": random_str,
                "metaKeywords": random_str,
                "order": 0
            }
        }
    );

    return result;
}