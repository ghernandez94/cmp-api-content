Feature: Test Category

  Background:
    * url baseUrl
    * def data_utilities = call read("classpath:util/data-util.js")

  Scenario: Get a Category | Status 200
    * def feature_create_category = karate.call("create-category.feature@reusable_creation")
    * def id_tenant = feature_create_category.tenant
    * def id_category = feature_create_category.response.id

    Given path 'v1', id_tenant, 'categories', id_category
    When method get
    Then status 200
    And match response.id == id_category

  Scenario: Get a Category by ID | Status 400 | UUID format
    * def id_tenant = karate.call("classpath:util/companies/create-company.feature@reusable_creation").response.id
    * def id_category = 'test-1234'
    Given path 'v1', id_tenant, 'categories', id_category
    When method get
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'

  Scenario: Get a Category by ID | Status 400 | Disallowed include field
    * def feature_create_category = karate.call("create-category.feature@reusable_creation")
    * def id_tenant = feature_create_category.tenant
    * def id_category = feature_create_category.response.id

    Given path 'v1', id_tenant, 'categories', id_category
    And param include = 'test-1234'
    When method get
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'

  Scenario: Get a Category by ID | Status 404 | Non-existent Category
    * def id_tenant = karate.call("classpath:util/companies/create-company.feature@reusable_creation").response.id
    * def random_category = data_utilities.random_uuid()
    Given path 'v1', id_tenant, 'categories', random_category
    When method get
    Then status 404
    And match response.message == 'Resource not found'

    
  Scenario: Get a Category by ID | Status 404 | Non-existent Tenant
    * def random_tenant = data_utilities.random_uuid()
    * def random_category = data_utilities.random_uuid()
    Given path 'v1', random_tenant, 'categories', random_category
    When method get
    Then status 404
    And match response.message == 'Resource not found'