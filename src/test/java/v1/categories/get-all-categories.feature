Feature: Test Categories

  Background:
    * url baseUrl
    * def created_category = karate.call("create-category.feature@reusable_creation");
    * def tenant = created_category.tenant
    * def data_utilities = call read("classpath:util/data-util.js")

  Scenario: Get all categories | Status 200
    Given path 'v1', tenant, 'categories'
    When method get
    Then status 200
    And match response.data == '#array'

  Scenario: Get all categories | Status 400 | Negative parameters
    Given path 'v1', tenant, 'categories'
    And param page = -1
    And param limit = -1
    When method get
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'

  Scenario: Get all categories | Status 400 | UUID validation
    Given path 'v1', '123', 'categories'
    When method get
    Then status 404
    #And match response.message == 'Bad request parameters'
    #And match response.errors == '#array'

  Scenario: Get all categories | Status 404 | Non-existent tenant
    * def random_tenant = data_utilities.random_uuid()
    Given path 'v1', random_tenant, 'categories'
    When method get
    Then status 200
    #And match response.message == 'Resource not found'
  