Feature: Test Categories

  Background:
    * url baseUrl
    * def create_201 = karate.callSingle('data/create-category-201.js')
    * def create_400 = karate.callSingle('data/create-category-400.js')
    * def data_utilities = call read("classpath:util/data-util.js")

  @ignore @reusable_creation
  Scenario: Create a Category | Status 201
    * def tenant = create_201[0].tenant
    * def new_category = create_201[0].body
    Given path 'v1', tenant, 'categories'
    And request new_category
    When method post
    Then status 201
    
  @201
  Scenario Outline: Create a Category | Status 201 | <case-description>
    Given path 'v1', '<tenant>', 'categories'
    And request <body>
    When method post
    Then status 201
    Examples:
      | create_201 |

  @400
  Scenario Outline: Create a Category | Status 400 | <case-description>
    Given path 'v1', '<tenant>', 'categories'
    And request <body>
    When method post
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'
    Examples:
      | create_400 |

  @400
  Scenario: Create a Category | Status 400 | Duplicated unique field (key)
    * def tenant = create_201[0].tenant
    * def random_str = data_utilities.random_text()
    * def new_category =
    """
      {
        "key": #(random_str),
        "name": #(random_str),
        "description": #(random_str)
      }
    """

    Given path 'v1', tenant, 'categories'
    And request new_category
    When method post
    Then status 201

    Given path 'v1', tenant, 'categories'
    And request new_category
    When method post
    Then status 400
    And match response.message == 'Bad request parameters'
    And match response.errors == '#array'