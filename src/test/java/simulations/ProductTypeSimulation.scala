package simulations;

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._
import io.gatling.http.Predef.http
import scala.concurrent.duration._

class ProductTypeSimulation extends Simulation {
  val httpConf = http.baseUrl(System.getProperty("baseUrl"))

  val list = scenario("Get All Product Types")
    .exec(karateFeature("classpath:v1/product-types/get-all-product-types.feature@GetAll"))

  setUp(
    list.inject(constantUsersPerSec(20) during (5 seconds) randomized).protocols(httpConf)
  ).assertions(
    global.responseTime.max.lt(50),
    global.successfulRequests.percent.gt(95)
  )
}